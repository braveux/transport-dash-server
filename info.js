module.exports = {
  metro: {
    route: 'http://api.wmata.com',
    key: 'api_key=j73zrdxmnbvee37bbans2e45',
    rail: {
      stationPrediction: '/StationPrediction.svc/json/GetPrediction/'
    },
    bus: {

    }
  } ,
  capitalBikeshare: {
    route: 'https://www.capitalbikeshare.com/data/stations/bikeStations.xml'
  },

  wunderground: {
    route: 'http://api.wunderground.com/api/09977be0a9de4817/conditions/q/DC/WASHINGTON.json',
    hourly: {
      route: 'http://api.wunderground.com/api/09977be0a9de4817/hourly/q/DC/WASHINGTON.json'
    }
  } 

}
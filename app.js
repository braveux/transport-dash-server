var express = require('express')
var app = express()
var bodyParser = require('body-parser')
var cookieParser = require('cookie-parser')
var cors = require('cors')
var request = require('request')
var parseString = require('xml2js').parseString;
var router = express.Router()

app.use(bodyParser.json())
app.use(bodyParser.urlencoded())
app.use(cookieParser())

var metro = require('./info').metro
var capitalBikeshare = require('./info').capitalBikeshare
var weather = require('./info').wunderground


router.get('/api/metro/prediction', cors(), function(req, res) {
  var route = ''
  route = route.concat(metro.route, metro.rail.stationPrediction, 'A02,A03,C03', '?', metro.key)
  request(route, function(error, response, body) {
    if (error) res.send(error)
    body = JSON.parse(body)
    res.send(body.Trains)
  })
})


router.get('/api/capitalbikeshare', cors(),  function(req, res) {
  var route = ''
  route = capitalBikeshare.route
  request(route, function(error, response, body) {
    if (error) res.send(error)
    parseString(body, function(xmlErr, result) {
      if (xmlErr) res.send(xmlErr)
      res.json(result.stations.station)
    })
  })
})

router.get('/api/weather/:location', cors(), function(req, res) {
  var route = ''
  route = weather.route
  request(route, function(error, response, body) {
    if (error) res.send(error)
    body = JSON.parse(body)
    res.json(body.current_observation)
  })
})

router.get('/api/weather/hourly/:location', cors(), function(req, res) {
  var route = ''
  route = weather.hourly.route
  request(route, function(error, response, body) {
    if (error) res.send(error)
    body = JSON.parse(body)
    res.json(body.hourly_forecast)
  })
}) 


app.use(router)
app.listen(3000, function() {
  console.log('It is werking on 3000')
})